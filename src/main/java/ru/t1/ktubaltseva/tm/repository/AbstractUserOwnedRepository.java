package ru.t1.ktubaltseva.tm.repository;

import ru.t1.ktubaltseva.tm.api.repository.IUserOwnedRepository;
import ru.t1.ktubaltseva.tm.model.AbstractUserOwnedModel;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    public M add(final String userId, final M model) {
        if (userId == null || userId.isEmpty()) return null;
        model.setUserId(userId);
        return add(model);
    }

    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) return;
        List<M> userModels = findAll(userId);
        userModels.removeAll(userModels);
    }

    public boolean existsById(final String userId, final String id) {
        return findOneById(userId, id) != null;
    }

    public List<M> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return models
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    public M findOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        return models
                .stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    public M findOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) return null;
        return models
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .skip(index)
                .findFirst()
                .orElse(null);
    }

    public int getSize(final String userId) {
        if (userId == null || userId.isEmpty()) return 0;
        return (int) models
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .count();
    }

    public void removeAll(final String userId) {
        models.removeAll(findAll(userId));
    }

    public M removeOne(final String userId, final M model) {
        if (model == null) return null;
        return removeById(userId, model.getId());
    }

    public M removeById(final String userId, final String id) {
        final M model = findOneById(userId, id);
        if (model == null) return null;
        return removeOne(model);
    }

    public M removeByIndex(final String userId, final Integer index) {
        final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return removeOne(model);
    }
}
