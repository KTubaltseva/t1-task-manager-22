package ru.t1.ktubaltseva.tm.repository;

import ru.t1.ktubaltseva.tm.api.repository.ITaskRepository;
import ru.t1.ktubaltseva.tm.model.Task;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public Task create(final String userId, final String name, final String description) {
        final Task task = new Task(name, description);
        return add(userId, task);
    }

    @Override
    public Task create(final String userId, final String name) {
        final Task task = new Task(name);
        return add(userId, task);
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return models
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .filter(m -> projectId.equals(m.getProjectId()))
                .collect(Collectors.toList());
    }

    @Override
    public void removeAllByProjectId(String userId, String projectId) {
        models.removeAll(findAllByProjectId(userId, projectId));
    }

}
