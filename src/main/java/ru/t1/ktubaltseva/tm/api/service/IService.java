package ru.t1.ktubaltseva.tm.api.service;

import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.AbstractEntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.IndexIncorrectException;
import ru.t1.ktubaltseva.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IService<M extends AbstractModel> {

    M add(M model) throws AbstractEntityNotFoundException;

    void clear();

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    M findOneById(String id) throws IdEmptyException, EntityNotFoundException;

    M findOneByIndex(Integer index) throws IndexIncorrectException, EntityNotFoundException;

    void removeAll();

    M removeOne(M model) throws EntityNotFoundException, UserNotFoundException;

    M removeById(String id) throws IdEmptyException;

    M removeByIndex(Integer index) throws IndexIncorrectException;

}
