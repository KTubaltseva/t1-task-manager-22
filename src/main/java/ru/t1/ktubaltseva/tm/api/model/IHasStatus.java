package ru.t1.ktubaltseva.tm.api.model;

import ru.t1.ktubaltseva.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(final Status status);

}
