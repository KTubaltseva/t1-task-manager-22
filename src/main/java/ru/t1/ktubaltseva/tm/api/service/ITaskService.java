package ru.t1.ktubaltseva.tm.api.service;

import ru.t1.ktubaltseva.tm.enumerated.Sort;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.field.NameEmptyException;
import ru.t1.ktubaltseva.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    Task create(String userId, String name, String description) throws AbstractException;

    Task create(String userId, String name) throws NameEmptyException, AuthRequiredException;

    Task changeTaskStatusById(
            String userId,
            String id,
            Status status
    ) throws AbstractException;

    Task changeTaskStatusByIndex(
            String userId,
            Integer index,
            Status status
    ) throws AbstractException;

    List<Task> findAll(String userId, Sort sort) throws AuthRequiredException;

    List<Task> findAllByProjectId(String userId, String projectId) throws AuthRequiredException;

    Task updateById(
            String userId,
            String id,
            String name,
            String description
    ) throws AbstractException;

    Task updateByIndex(
            String userId,
            Integer index,
            String name,
            String description
    ) throws AbstractException;

}
