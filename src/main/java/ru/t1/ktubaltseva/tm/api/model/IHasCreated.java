package ru.t1.ktubaltseva.tm.api.model;

import java.util.Date;

public interface IHasCreated {

    Date getCreated();

}
