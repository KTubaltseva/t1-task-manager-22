package ru.t1.ktubaltseva.tm.api.service;

import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.exception.entity.entityAlreadyExists.AbstractAlreadyExistsException;
import ru.t1.ktubaltseva.tm.exception.entity.entityAlreadyExists.LoginAlreadyExistsException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.AbstractFieldException;
import ru.t1.ktubaltseva.tm.exception.field.EmailEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.LoginEmptyException;
import ru.t1.ktubaltseva.tm.model.User;

public interface IUserService extends IService<User> {

    User create(
            String login,
            String password
    ) throws AbstractFieldException, AbstractAlreadyExistsException;

    User create(
            String login,
            String password,
            String email
    ) throws AbstractFieldException, AbstractAlreadyExistsException;

    User create(
            String login,
            String password,
            Role role
    ) throws AbstractFieldException, LoginAlreadyExistsException;

    User findByLogin(String login) throws LoginEmptyException, UserNotFoundException;

    User findByEmail(String email) throws EmailEmptyException, UserNotFoundException;

    Boolean isLoginExists(String login) throws LoginEmptyException;

    Boolean isEmailExists(String email) throws EmailEmptyException;

    void lockUserByLogin(String login) throws UserNotFoundException, LoginEmptyException;

    User removeByLogin(String login) throws UserNotFoundException, LoginEmptyException;

    User removeByEmail(String email) throws EmailEmptyException, UserNotFoundException;

    User setPassword(
            String id,
            String password
    ) throws UserNotFoundException, AbstractFieldException;

    void unlockUserByLogin(String login) throws UserNotFoundException, LoginEmptyException;

    User updateUser(
            String id,
            String firstName,
            String middleName,
            String lastName
    ) throws UserNotFoundException, IdEmptyException;

}
