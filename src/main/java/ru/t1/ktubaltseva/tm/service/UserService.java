package ru.t1.ktubaltseva.tm.service;

import ru.t1.ktubaltseva.tm.api.repository.IProjectRepository;
import ru.t1.ktubaltseva.tm.api.repository.ITaskRepository;
import ru.t1.ktubaltseva.tm.api.repository.IUserRepository;
import ru.t1.ktubaltseva.tm.api.service.IUserService;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.exception.entity.entityAlreadyExists.AbstractAlreadyExistsException;
import ru.t1.ktubaltseva.tm.exception.entity.entityAlreadyExists.EmailAlreadyExistsException;
import ru.t1.ktubaltseva.tm.exception.entity.entityAlreadyExists.LoginAlreadyExistsException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.*;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.User;
import ru.t1.ktubaltseva.tm.util.HashUtil;

import java.util.List;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public UserService(
            final IUserRepository modelRepository,
            final ITaskRepository taskRepository,
            final IProjectRepository projectRepository
    ) {
        super(modelRepository);
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public User create(
            final String login,
            final String password
    ) throws AbstractFieldException, LoginAlreadyExistsException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isLoginExists(login)) throw new LoginAlreadyExistsException(login);
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(Role.USUAL);
        return modelRepository.add(user);
    }

    @Override
    public User create(
            final String login,
            final String password,
            final String email
    ) throws AbstractFieldException, AbstractAlreadyExistsException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isLoginExists(login)) throw new LoginAlreadyExistsException(login);
        if (isEmailExists(login)) throw new EmailAlreadyExistsException(email);
        final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(
            final String login,
            final String password,
            final Role role
    ) throws AbstractFieldException, LoginAlreadyExistsException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (modelRepository.isLoginExists(login)) throw new LoginAlreadyExistsException(login);
        final User user = create(login, password);
        user.setRole(role);
        return user;
    }

    @Override
    public User findByLogin(final String login) throws LoginEmptyException, UserNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = modelRepository.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public User findByEmail(final String email) throws EmailEmptyException, UserNotFoundException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        final User user = modelRepository.findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public Boolean isLoginExists(String login) throws LoginEmptyException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return modelRepository.isLoginExists(login);
    }

    @Override
    public Boolean isEmailExists(String email) throws EmailEmptyException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return modelRepository.isEmailExists(email);
    }

    @Override
    public void lockUserByLogin(final String login) throws UserNotFoundException, LoginEmptyException {
        final User user = findByLogin(login);
        user.setLocked(true);
    }

    @Override
    public User removeOne(final User model) throws UserNotFoundException {
        if (model == null) throw new UserNotFoundException();
        final String userId = model.getId();
        final List<Project> projects = projectRepository.findAll(userId);
        for (Project project : projects) {
            taskRepository.removeAllByProjectId(userId, project.getId());
        }
        projectRepository.removeAll(userId);
        try {
            return super.removeOne(model);
        } catch (EntityNotFoundException e) {
            throw new UserNotFoundException();
        }
    }

    @Override
    public User removeByLogin(final String login) throws UserNotFoundException, LoginEmptyException {
        final User user = findByLogin(login);

        return modelRepository.removeOne(user);
    }

    @Override
    public User removeByEmail(final String email) throws EmailEmptyException, UserNotFoundException {
        final User user = findByEmail(email);
        return modelRepository.removeOne(user);
    }

    @Override
    public User setPassword(final String id, final String password) throws AbstractFieldException, UserNotFoundException {
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        User user;
        try {
            user = findOneById(id);
        } catch (EntityNotFoundException e) {
            throw new UserNotFoundException();
        }
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public void unlockUserByLogin(final String login) throws UserNotFoundException, LoginEmptyException {
        final User user = findByLogin(login);
        user.setLocked(false);
    }

    @Override
    public User updateUser(
            final String id,
            final String firstName,
            final String middleName,
            final String lastName
    ) throws IdEmptyException, UserNotFoundException {
        User user;
        try {
            user = findOneById(id);
        } catch (EntityNotFoundException e) {
            throw new UserNotFoundException();
        }
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        return user;
    }

}
