package ru.t1.ktubaltseva.tm.service;

import ru.t1.ktubaltseva.tm.api.repository.IProjectRepository;
import ru.t1.ktubaltseva.tm.api.service.IProjectService;
import ru.t1.ktubaltseva.tm.enumerated.Sort;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.ProjectNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.AbstractFieldException;
import ru.t1.ktubaltseva.tm.exception.field.DescriptionEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.NameEmptyException;
import ru.t1.ktubaltseva.tm.model.Project;

import java.util.List;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(IProjectRepository modelRepository) {
        super(modelRepository);
    }

    @Override
    public Project changeProjectStatusById(
            final String userId,
            final String id,
            final Status status
    ) throws AbstractException {
        Project project;
        try {
            project = findOneById(userId, id);
        } catch (EntityNotFoundException e) {
            throw new ProjectNotFoundException();
        }
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(
            final String userId,
            final Integer index,
            final Status status
    ) throws AbstractException {
        Project project;
        try {
            project = findOneByIndex(userId, index);
        } catch (EntityNotFoundException e) {
            throw new ProjectNotFoundException();
        }
        project.setStatus(status);
        return project;
    }

    @Override
    public Project create(
            final String userId,
            final String name,
            final String description
    ) throws AbstractFieldException, AuthRequiredException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return modelRepository.create(userId, name, description);
    }

    @Override
    public Project create(
            final String userId, final String name) throws NameEmptyException, AuthRequiredException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return modelRepository.create(userId, name);
    }

    @Override
    public List<Project> findAll(
            final String userId, final Sort sort) throws AuthRequiredException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (sort == null) return findAll(userId);
        return modelRepository.findAll(userId, sort.getComparator());
    }

    @Override
    public Project updateById(
            final String userId,
            final String id,
            final String name,
            final String description
    ) throws AbstractException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        Project project;
        try {
            project = findOneById(userId, id);
        } catch (EntityNotFoundException e) {
            throw new ProjectNotFoundException();
        }
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(
            final String userId,
            final Integer index,
            final String name,
            final String description
    ) throws AbstractException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        Project project;
        try {
            project = findOneByIndex(userId, index);
        } catch (EntityNotFoundException e) {
            throw new ProjectNotFoundException();
        }
        project.setName(name);
        project.setDescription(description);
        return project;
    }

}
