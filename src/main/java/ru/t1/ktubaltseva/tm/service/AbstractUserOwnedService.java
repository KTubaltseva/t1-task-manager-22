package ru.t1.ktubaltseva.tm.service;

import ru.t1.ktubaltseva.tm.api.repository.IUserOwnedRepository;
import ru.t1.ktubaltseva.tm.api.service.IUserOwnedService;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.IndexIncorrectException;
import ru.t1.ktubaltseva.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(R modelRepository) {
        super(modelRepository);
    }

    public M add(final String userId, final M model) throws EntityNotFoundException, AuthRequiredException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (model == null) throw new EntityNotFoundException();
        modelRepository.add(userId, model);
        return model;
    }

    public void clear(final String userId) throws AuthRequiredException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        modelRepository.clear(userId);
    }

    public List<M> findAll(final String userId) throws AuthRequiredException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        return modelRepository.findAll(userId);
    }

    public List<M> findAll(final String userId, final Comparator<M> comparator) throws AuthRequiredException {
        if (comparator == null) return findAll(userId);
        return modelRepository.findAll(userId, comparator);
    }

    public M findOneById(final String userId, final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final M model = modelRepository.findOneById(userId, id);
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    public M findOneByIndex(final String userId, final Integer index) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= modelRepository.getSize(userId)) throw new IndexIncorrectException();
        final M model = modelRepository.findOneByIndex(userId, index);
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    @Override
    public void removeAll(String userId) {
        modelRepository.removeAll(userId);
    }

    public M removeOne(final String userId, final M model) throws EntityNotFoundException, AuthRequiredException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (model == null) throw new EntityNotFoundException();
        modelRepository.removeOne(userId, model);
        return model;
    }

    public M removeById(final String userId, final String id) throws IdEmptyException, AuthRequiredException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return modelRepository.removeById(userId, id);
    }

    public M removeByIndex(final String userId, final Integer index) throws IndexIncorrectException, AuthRequiredException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= modelRepository.getSize(userId)) throw new IndexIncorrectException();
        return modelRepository.removeByIndex(userId, index);
    }

}
