package ru.t1.ktubaltseva.tm.exception.entity.entityAlreadyExists;

public final class EmailAlreadyExistsException extends AbstractAlreadyExistsException {

    public EmailAlreadyExistsException() {
        super("Error! Email already exists...");
    }

    public EmailAlreadyExistsException(String message) {
        super("Error! Email \"" + message + "\" already exists...");
    }
}
