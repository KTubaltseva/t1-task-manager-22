package ru.t1.ktubaltseva.tm.exception.entity.entityNotFound;

public final class EntityNotFoundException extends AbstractEntityNotFoundException {

    public EntityNotFoundException() {
        super("Error! Entity not found...");
    }

    public EntityNotFoundException(String message) {
        super("Error! Entity " + message + " not found...");
    }
}
