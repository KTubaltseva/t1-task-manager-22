package ru.t1.ktubaltseva.tm.model;

import java.util.UUID;

public class AbstractModel {

    private final String id = UUID.randomUUID().toString();

    public String getId() {
        return id;
    }

}
