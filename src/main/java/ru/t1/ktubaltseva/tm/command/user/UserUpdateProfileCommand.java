package ru.t1.ktubaltseva.tm.command.user;

import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.model.User;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

public class UserUpdateProfileCommand extends AbstractUserCommand {

    private final String NAME = "user-update-profile";

    private final String DESC = "Update user profile.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[UPDATE USER PROFILE]");
        System.out.println("ENTER FIRST NAME:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        final String middleName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME:");
        final String lastName = TerminalUtil.nextLine();
        final User user = getUserService().updateUser(
                getUserId(),
                firstName,
                middleName,
                lastName
        );
        displayUser(user);
    }

}
