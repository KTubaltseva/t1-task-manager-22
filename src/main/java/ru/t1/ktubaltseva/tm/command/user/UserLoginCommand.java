package ru.t1.ktubaltseva.tm.command.user;

import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

public class UserLoginCommand extends AbstractUserCommand {

    private final String NAME = "user-login";

    private final String DESC = "Login user.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[LOGIN USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        getAuthService().login(login, password);
    }

}
