package ru.t1.ktubaltseva.tm.command.task;

import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.model.Task;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

public final class TaskDisplayByIdCommand extends AbstractTaskCommand {

    private final String NAME = "task-display-by-id";

    private final String DESC = "Display task by Id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DISPLAY PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = getTaskService().findOneById(getUserId(), id);
        displayTask(task);
    }

}
