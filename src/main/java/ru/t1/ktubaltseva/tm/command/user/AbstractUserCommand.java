package ru.t1.ktubaltseva.tm.command.user;

import ru.t1.ktubaltseva.tm.command.AbstractCommand;
import ru.t1.ktubaltseva.tm.model.User;

import java.util.List;

public abstract class AbstractUserCommand extends AbstractCommand {

    @Override
    public String getArgument() {
        return null;
    }

    protected void renderUsers(final List<User> users) {
        int index = 1;
        for (final User user : users) {
            System.out.println(index + ". " + user);
            System.out.println();
            index++;
        }
    }

    public void displayUser(final User user) {
        if (user == null) return;
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("ROLE: " + user.getRole());
    }

    protected void renderUsersFullInfo(final List<User> users) {
        int index = 1;
        for (final User user : users) {
            System.out.println(index + ".");
            displayUser(user);
            System.out.println();
            index++;
        }
    }

}